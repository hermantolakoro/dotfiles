call plug#begin('~/.config/nvim/plugged')
	" Theme
	Plug 'morhetz/gruvbox'
	Plug 'dracula/vim', { 'as': 'dracula' }
	Plug 'sonph/onehalf', { 'rtp': 'vim' }


	" Keyboard Arrow
	Plug 'aaren/arrowkeyrepurpose'	

	Plug 'sheerun/vim-polyglot'
	
	" File and folder management
	Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
	Plug 'junegunn/fzf.vim'
	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
	Plug 'preservim/nerdtree'
	Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

	" Snippets
	Plug 'SirVer/ultisnips'
	Plug 'honza/vim-snippets'
	" Plug 'natebosch/dartlang-snippets'

	" Language support
	Plug 'tpope/vim-projectionist'
	Plug 'neoclide/coc.nvim', {'branch': 'release'}
	Plug 'jiangmiao/auto-pairs'

	" Dart & Flutter
	" Plug 'dart-lang/dart-vim-plugin'
	" Plug 'thosakwe/vim-flutter'


	" Typescript
	Plug 'ianks/vim-tsx'
	Plug 'leafgarland/typescript-vim'

	" Git
	Plug 'tpope/vim-fugitive'
	" Plug 'vim-airline/vim-airline'
	" Plug 'vim-airline/vim-airline-themes'
	
	" Vim Lualine
	Plug 'nvim-lualine/lualine.nvim'
	" If you want to have icons in your statusline choose one of these
	Plug 'kyazdani42/nvim-web-devicons'


	Plug 'posva/vim-vue'
	
	" Vim Spring-Boot
	Plug 'vim-scripts/spring.vim'

	" Vim DefIcons
	Plug 'ryanoasis/vim-devicons'	
	
	" Syntax
	Plug 'vim-syntastic/syntastic'

	" Vim Comment
	Plug 'tpope/vim-commentary'

	Plug 'jbyuki/venn.nvim'
	
	" Plug 'voldikss/vim-floaterm'
	Plug 'akinsho/toggleterm.nvim', {'tag' : 'v2.*'}

	Plug 'pantharshit00/vim-prisma'

	" Plug 'aquach/vim-http-client'
	Plug 'NTBBloodbath/rest.nvim'	
	Plug 'romgrk/barbar.nvim'
	" Telescope 
	Plug 'nvim-lua/plenary.nvim'
	Plug 'nvim-telescope/telescope.nvim'	
	" Todo Comments
	Plug 'folke/todo-comments.nvim'

	" barbar
	Plug 'romgrk/barbar.nvim'	
	" nvim notify
	Plug 'rcarriga/nvim-notify'	

	" treesitter
	Plug 'nvim-treesitter/nvim-treesitter'	

	" If you don't have nodejs and yarn
	" use pre build, add 'vim-plug' to the filetype list so vim-plug can update this plugin
	" see: https://github.com/iamcco/markdown-preview.nvim/issues/50
	" Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

	" If you have nodejs and yarn
	Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }

	" IndentLine
	Plug 'vim-scripts/indentLine.vim'

	" Colorizer
	Plug 'norcalli/nvim-colorizer.lua'	

	" Vim testing
	Plug 'vim-test/vim-test'	

	" Dashboard 
	" Plug 'glepnir/dashboard-nvim'	
call plug#end()


colorscheme onehalfdark
set background=dark
syntax enable
" syntax enable
" colorscheme dracula
let g:airline_theme='onehalfdark'

set encoding=utf-8
set number
set relativenumber
set nowrap
set nohlsearch
set smartcase
set noswapfile
set nobackup
set title
set termguicolors
filetype plugin on
set undofile
set undodir=~/.config/vim/undodir
set incsearch
set tabstop=2
set softtabstop=0 noexpandtab
set shiftwidth=2
" set colorcolumn=120
set cursorline
set clipboard=unnamedplus
set backspace=indent,eol,start
highlight ColorColumn ctermbg=0 guibg=lightgrey
imap jj <Esc>
cmap jj <c-c>
vmap v <Esc>
map ; :
let g:mapleader=','
nnoremap <leader><leader> <c-^>

map <CR> :nohl<CR>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

nnoremap / /\v
vnoremap / /\v

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

let mapleader=" "
nnoremap <leader>fe :CocCommand flutter.emulators <CR>
nnoremap <leader>fd :below new output:///flutter-dev <CR>

nnoremap <C-b> :NERDTreeToggle<CR>

" let g:dart_format_on_save = 1
" let g:dartfmt_options = ['--fix', '--line-length 120']

" Coc
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)


" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction


nmap <C-P> :FZF<CR>

nmap <leader>gs :G<CR>
nmap <leader>gh :diffget //2<CR>
nmap <leader>gl :diffget //3<CR>

imap <tab> <Plug>(coc-snippets-expand)
let g:UltiSnipsExpandTrigger = '<Nop>'
let g:coc_snippet_next = '<TAB>'
let g:coc_snippet_prev = '<S-TAB>'

inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a <Plug>(coc-codeaction-selected)
nmap <leader>a <Plug>(coc-codeaction-selected)

"coc config
let g:coc_global_extensions = [
  \ 'coc-snippets',
  \ 'coc-tsserver',
  \ 'coc-eslint',
  \ 'coc-prettier',
  \ 'coc-json',
  \ 'coc-flutter',
  \ 'coc-snippets',
  \ 'coc-yaml',
  \ 'coc-tslint-plugin',
  \ 'coc-tsserver',
  \ 'coc-emmet',
  \ 'coc-css',
  \ 'coc-html',
  \ 'coc-json',
\ 'coc-java',
\ 'coc-java-debug',
\ 'coc-css',
\ 'coc-docker',
  \ ]

let g:NERDTreeGitStatusWithFlags = 1
let NERDTreeShowHidden=1

" == AUTOCMD ================================
" by default .ts file are not identified as typescript and .tsx files are not
" identified as typescript react file, so add following
au BufNewFile,BufRead *.ts setlocal filetype=typescript
au BufNewFile,BufRead *.tsx setlocal filetype=typescript.tsx
" == AUTOCMD END ================================

let g:syntastic_java_checkers = ['checkstyle']
let g:syntastic_java_checkstyle_classpath = '~/.config/vim/checkstyle-10.0-all.jar'
let g:syntastic_java_checkstyle_conf_file = '~/.config/vim/checkstyle.xml'


set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Airline Config
" let g:airline#extensions#tabline#enabled = 1
" let g:airline#extensions#tabline#left_sep = ' '
" let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_theme='wombat'

" Mapping TABS
nnoremap <leader><Left> :bp<CR>
nnoremap <leader><Right> :bn<CR>
nnoremap <leader><Up> :bf<CR>
nnoremap <leader><Down> :bl<CR>

" Mapping FloatTerminal
" let g:floaterm_keymap_new    = '<F7>'
" let g:floaterm_keymap_prev   = '<F8>'
" let g:floaterm_keymap_next   = '<F9>'
" let g:floaterm_keymap_toggle = '<F12>'

" FLUTTER CONFIG

" Some of these key choices were arbitrary;
" it's just an example.
nnoremap <leader>fa :FlutterRun<cr>
nnoremap <leader>fq :FlutterQuit<cr>
nnoremap <leader>fr :FlutterHotReload<cr>
nnoremap <leader>fR :FlutterHotRestart<cr>
nnoremap <leader>fD :FlutterVisualDebug<cr>


" COC Rest Client Config
noremap <Leader>0 :CocCommand rest-client.request <cr>

" format coc prettier
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Config Telescope 
" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

" Config Notify
lua << EOF
vim.notify = require("notify")
-- lualine setup
require('lualine').setup {
	icons_enabled = true,
	component_separators = { left = '', right = ''},
	section_separators = { left = '', right = ''},
	theme = 'dracula'
}

require("toggleterm").setup{}

-- Treesitter Config
require'nvim-treesitter.configs'.setup {
  ensure_installed = "all", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  highlight = {
    enable = true,              -- false will disable the whole extension
		disable = { "dart" },
  },
}

require'colorizer'.setup()

-- rest config
require("rest-nvim").setup{
			 -- Open request results in a horizontal split
      result_split_horizontal = false,
      -- Keep the http file buffer above|left when split horizontal|vertical
      result_split_in_place = false,
      -- Skip SSL verification, useful for unknown certificates
      skip_ssl_verification = false,
      -- Highlight request on run
      highlight = {
        enabled = true,
        timeout = 150,
      },
      result = {
        -- toggle showing URL, HTTP info, headers at top the of result window
        show_url = true,
        show_http_info = true,
        show_headers = true,
      },
      -- Jump to request line on run
      jump_to_request = false,
      env_file = '.env',
      custom_dynamic_variables = {},
      yank_dry_run = true,
}



EOF

" Barbar config
" Move to previous/next
nnoremap <silent>    <A-,> <Cmd>BufferPrevious<CR>
nnoremap <silent>    <A-.> <Cmd>BufferNext<CR>
" Re-order to previous/next
nnoremap <silent>    <A-<> <Cmd>BufferMovePrevious<CR>
nnoremap <silent>    <A->> <Cmd>BufferMoveNext<CR>
" Goto buffer in position...
nnoremap <silent>    <A-1> <Cmd>BufferGoto 1<CR>
nnoremap <silent>    <A-2> <Cmd>BufferGoto 2<CR>
nnoremap <silent>    <A-3> <Cmd>BufferGoto 3<CR>
nnoremap <silent>    <A-4> <Cmd>BufferGoto 4<CR>
nnoremap <silent>    <A-5> <Cmd>BufferGoto 5<CR>
nnoremap <silent>    <A-6> <Cmd>BufferGoto 6<CR>
nnoremap <silent>    <A-7> <Cmd>BufferGoto 7<CR>
nnoremap <silent>    <A-8> <Cmd>BufferGoto 8<CR>
nnoremap <silent>    <A-9> <Cmd>BufferGoto 9<CR>
nnoremap <silent>    <A-0> <Cmd>BufferLast<CR>
" Pin/unpin buffer
nnoremap <silent>    <A-p> <Cmd>BufferPin<CR>
" Close buffer
nnoremap <silent>    <A-c> <Cmd>BufferClose<CR>

" NOTE: If barbar's option dict isn't created yet, create it
let bufferline = get(g:, 'bufferline', {})

" Enable/disable animations
let bufferline.animation = v:true
" Enable/disable close button
let bufferline.closable = v:true
"  - middle-click: delete buffer
let bufferline.clickable = v:true
" Configure icons on the bufferline.
let bufferline.icon_separator_active = '▎'
let bufferline.icon_separator_inactive = '▎'
let bufferline.icon_close_tab = ''
let bufferline.icon_close_tab_modified = '●'
let bufferline.icon_pinned = '車'
let bufferline.semantic_letters = v:true

lua << EOF

local hl = require'bufferline.utils'.hl
local icons = require 'bufferline.icons'

return {
  --- Setup the highlight groups for this plugin.
  setup = function()

    --- @type barbar.util.Highlight
    local fg_target = {cterm = 'red'}
    fg_target.gui = fg_target.cterm

    local fg_current  = hl.fg_or_default({'Normal'}, '#efefef', 255)
    local fg_visible  = hl.fg_or_default({'TabLineSel'}, '#efefef', 255)
    local fg_inactive = hl.fg_or_default({'TabLineFill'}, '#888888', 102)

    local fg_modified = hl.fg_or_default({'WarningMsg'}, '#E5AB0E', 178)
    local fg_special  = hl.fg_or_default({'Special'}, '#599eff', 75)
    local fg_subtle = hl.fg_or_default({'NonText', 'Comment'}, '#555555', 240)

    local bg_current  = hl.bg_or_default({'Normal'}, 'none')
    local bg_visible  = hl.bg_or_default({'TabLineSel', 'Normal'}, 'none')
    local bg_inactive = hl.bg_or_default({'TabLineFill', 'StatusLine'}, 'none')

    --      Current: current buffer
    --      Visible: visible but not current buffer
    --     Inactive: invisible but not current buffer
    --        -Icon: filetype icon
    --       -Index: buffer index
    --         -Mod: when modified
    --        -Sign: the separator between buffers
    --      -Target: letter in buffer-picking mode
    hl.set_default('BufferCurrent',        bg_current, fg_current)
    hl.set_default('BufferCurrentIndex',   bg_current, fg_special)
    hl.set_default('BufferCurrentMod',     bg_current, fg_modified)
    hl.set_default('BufferCurrentSign',    bg_current, fg_special)
    hl.set_default('BufferCurrentTarget',  bg_current, fg_target, true)
    hl.set_default('BufferInactive',       bg_inactive, fg_inactive)
    hl.set_default('BufferInactiveIndex',  bg_inactive, fg_subtle)
    hl.set_default('BufferInactiveMod',    bg_inactive, fg_modified)
    hl.set_default('BufferInactiveSign',   bg_inactive, fg_subtle)
    hl.set_default('BufferInactiveTarget', bg_inactive, fg_target, true)
    hl.set_default('BufferTabpageFill',    bg_inactive, fg_inactive)
    hl.set_default('BufferTabpages',       bg_inactive, fg_special, true)
    hl.set_default('BufferVisible',        bg_visible, fg_visible)
    hl.set_default('BufferVisibleIndex',   bg_visible, fg_visible)
    hl.set_default('BufferVisibleMod',     bg_visible, fg_modified)
    hl.set_default('BufferVisibleSign',    bg_visible, fg_visible)
    hl.set_default('BufferVisibleTarget',  bg_visible, fg_target, true)

    hl.set_default_link('BufferCurrentIcon', 'BufferCurrent')
    hl.set_default_link('BufferInactiveIcon', 'BufferInactive')
    hl.set_default_link('BufferVisibleIcon', 'BufferVisible')
    hl.set_default_link('BufferOffset', 'BufferTabpageFill')

    icons.set_highlights()
  end
}

EOF

" End barbar config
"
" ToggleTerm Config
" set
autocmd TermEnter term://*toggleterm#*
      \ tnoremap <silent><c-t> <Cmd>exe v:count1 . "ToggleTerm"<CR>

" By applying the mappings this way you can pass a count to your
" mapping to open a specific window.
" For example: 2<C-t> will open terminal 2
nnoremap <silent><c-t> <Cmd>exe v:count1 . "ToggleTerm"<CR>
inoremap <silent><c-t> <Esc><Cmd>exe v:count1 . "ToggleTerm"<CR>


" Config coc-restclient
noremap <Leader>0 :CocCommand rest-client.request <cr>

" Markdown Config
" set to 1, nvim will open the preview window after entering the markdown buffer
" default: 0
let g:mkdp_auto_start = 0

" set to 1, the nvim will auto close current preview window when change
" from markdown buffer to another buffer
" default: 1
let g:mkdp_auto_close = 1

" set to 1, the vim will refresh markdown when save the buffer or
" leave from insert mode, default 0 is auto refresh markdown as you edit or
" move the cursor
" default: 0
let g:mkdp_refresh_slow = 0

" set to 1, the MarkdownPreview command can be use for all files,
" by default it can be use in markdown file
" default: 0
let g:mkdp_command_for_global = 0

" set to 1, preview server available to others in your network
" by default, the server listens on localhost (127.0.0.1)
" default: 0
let g:mkdp_open_to_the_world = 0

" use custom IP to open preview page
" useful when you work in remote vim and preview on local browser
" more detail see: https://github.com/iamcco/markdown-preview.nvim/pull/9
" default empty
let g:mkdp_open_ip = ''

" specify browser to open preview page
" for path with space
" valid: `/path/with\ space/xxx`
" invalid: `/path/with\\ space/xxx`
" default: ''
let g:mkdp_browser = ''

" set to 1, echo preview page url in command line when open preview page
" default is 0
let g:mkdp_echo_preview_url = 0

" a custom vim function name to open preview page
" this function will receive url as param
" default is empty
let g:mkdp_browserfunc = ''

" options for markdown render
" mkit: markdown-it options for render
" katex: katex options for math
" uml: markdown-it-plantuml options
" maid: mermaid options
" disable_sync_scroll: if disable sync scroll, default 0
" sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
"   middle: mean the cursor position alway show at the middle of the preview page
"   top: mean the vim top viewport alway show at the top of the preview page
"   relative: mean the cursor position alway show at the relative positon of the preview page
" hide_yaml_meta: if hide yaml metadata, default is 1
" sequence_diagrams: js-sequence-diagrams options
" content_editable: if enable content editable for preview page, default: v:false
" disable_filename: if disable filename header for preview page, default: 0
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:false,
    \ 'disable_filename': 0,
    \ 'toc': {}
    \ }

" use a custom markdown style must be absolute path
" like '/Users/username/markdown.css' or expand('~/markdown.css')
let g:mkdp_markdown_css = ''

" use a custom highlight style must absolute path
" like '/Users/username/highlight.css' or expand('~/highlight.css')
let g:mkdp_highlight_css = ''

" use a custom port to start server or empty for random
let g:mkdp_port = ''

" preview page title
" ${name} will be replace with the file name
let g:mkdp_page_title = '「${name}」'

" recognized filetypes
" these filetypes will have MarkdownPreview... commands
let g:mkdp_filetypes = ['markdown']

" set default theme (dark or light)
" By default the theme is define according to the preferences of the system
let g:mkdp_theme = 'light'
" End Markdown config

" indentLine config
let g:indentLine_color_term = 239
let g:indentLine_color_gui = '#5e5e5e'
let g:indentLine_char = '┆'


" Java Auto create class
autocmd BufNewFile *.java
 \ exe "normal O\n\npublic class " . expand('%:t:r') .
 \ " {\n\n}\n\<Esc>1G"

" Default value is clap
" let g:dashboard_default_executive ='clap'

" TODO CONFIG
lua << EOF
  require("todo-comments").setup {
    -- your configuration comes here
    -- or leave it empty to use the default settings
    -- refer to the configuration section below
		signs = true, -- show icons in the signs column
		sign_priority = 8, -- sign priority
		-- keywords recognized as todo comments
		keywords = {
			FIX = {
				icon = " ", -- icon used for the sign, and in search results
				color = "error", -- can be a hex color, or a named color (see below)
				alt = { "FIXME", "BUG", "FIXIT", "ISSUE" }, -- a set of other keywords that all map to this FIX keywords
				-- signs = false, -- configure signs for some keywords individually
			},
			TODO = { icon = " ", color = "info" },
			HACK = { icon = " ", color = "warning" },
			WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
			PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
			NOTE = { icon = " ", color = "hint", alt = { "INFO" } },
		},
		merge_keywords = true, -- when true, custom keywords will be merged with the defaults
		-- highlighting of the line containing the todo comment
		-- * before: highlights before the keyword (typically comment characters)
		-- * keyword: highlights of the keyword
		-- * after: highlights after the keyword (todo text)
		highlight = {
			before = "", -- "fg" or "bg" or empty
			keyword = "wide", -- "fg", "bg", "wide" or empty. (wide is the same as bg, but will also highlight surrounding characters)
			after = "fg", -- "fg" or "bg" or empty
			pattern = [[.*<(KEYWORDS)\s*:]], -- pattern or table of patterns, used for highlightng (vim regex)
			comments_only = true, -- uses treesitter to match keywords in comments only
			max_line_len = 400, -- ignore lines longer than this
			exclude = {}, -- list of file types to exclude highlighting
		},
		-- list of named colors where we try to extract the guifg from the
		-- list of hilight groups or use the hex color if hl not found as a fallback
		colors = {
			error = { "DiagnosticError", "ErrorMsg", "#DC2626" },
			warning = { "DiagnosticWarning", "WarningMsg", "#FBBF24" },
			info = { "DiagnosticInfo", "#2563EB" },
			hint = { "DiagnosticHint", "#10B981" },
			default = { "Identifier", "#7C3AED" },
		},
		search = {
			command = "rg",
			args = {
				"--color=never",
				"--no-heading",
				"--with-filename",
				"--line-number",
				"--column",
			},
			-- regex that will be used to match keywords.
			-- don't replace the (KEYWORDS) placeholder
			pattern = [[\b(KEYWORDS):]], -- ripgrep regex
			-- pattern = [[\b(KEYWORDS)\b]], -- match without the extra colon. You'll likely get false positives
		},
  }
EOF

augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source %
augroup END
